+++

topics  = ["internets","networking","sysadmin"]  
title   = "{{ replace .TranslationBaseName "-" " " | title }}"
date    = {{ .Date }}
type    = "post"  
author  = "will"  
tags    = ["internets","linux"]  

+++
