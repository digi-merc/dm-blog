# personal blog

[![pipeline status](https://gitlab.com/digi-merc/dm-blog/badges/master/pipeline.svg)](https://gitlab.com/digi-merc/dm-blog/commits/master)

Pull master, add content as new branches. Recommend using `hugo server --disableFastRender` to locally preview while writing.
  * Note that the disqus comments **WILL NOT** load when previewing locally, thanks to a conditional in the layout partial .js

bootstrap and materialize css are included for styling use.

Request merge with master, approval triggers CI build and site push to live host.

---

Built with [Hugo](https://gohugo.io)

Theme based on [strange case](https://themes.gohugo.io/strange-case/) but heavily modified to add features like tag heatmap  
