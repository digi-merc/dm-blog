+++
author = "will"
date = "2017-06-22T17:27:09-05:00"
tags = ["personal","vacation"]
title = "College visiting"
topics = ["roadtrip"]
type = "post"

+++

# Yale is impressive...

Connecticut is beautiful, no doubt. We drove along a river, through little towns with hillside homes and restaurants that are so very different from what we're used to in Texas. As an added bonus, it _wasn't_ 100+ degrees, so we could (and did) drive with windows open and arms poked right out, literally touching this New World.

We spent the night in a hotel right on a small inlet off of Long Island Sound, and had the smell of the salt water and the sound of boats all night. It was pretty nice, honestly.

---

Not as nice as Yale itself, though.

<img src="/img/2017/06/yale/cross_campus.jpg" alt="Cross Campus" style="width: 100%; height: 100%"/>
<div align="center">Cross Campus, one of the many quad-type areas</div>

---

<img src="/img/2017/06/yale/sterling.jpg" alt="Sterling Memorial Library" style="width: 100%; height: 100%"/>
<div align="center">Sterling Memorial Library, or at least the front entrance.</div>

---

Zoe has more photos, I think, but I was distracted by the staggering amount of gothic architecture.

Perhaps my favorite place on the tour, not surprisingly, was the Beinecke Rare Book & Manuscript Library.

<img src="/img/2017/06/yale/beinecke_outside.jpg" alt="Outside Shot" style="width: 100%; height: 100%"/>
<div align="center">The outside of the Library, one of the many they have on campus.</div>

Built in the 60s, the squares are single sheets of marble, cut to an exact 1.25" thickness that allows natural light through, but stops ink and paper-damaging UV light.

<img src="/img/2017/06/yale/beinecke_inside.jpg" alt="Outside Shot" style="width: 100%; height: 100%"/>
<div align="center">The main archive.</div>

Students don't go into the archive themselves, however. They wait in an underground reading room, for an archivist to bring them the book they need, which could be anything from original Audubon books, a 15th century Gutenberg Bible, or [many others][1], including papyrus scrolls.

---

Perhaps the best news was that they have expanded their student capacity, so that starting this fall they will be able to accept 6000 new students, instead of the usual 5400. Zoe is pretty excited, and honestly so am I; I might move up here sometime, it's damn nice.

We still have her actual conference to go, however a cursory drive around Boston shows that it's less interesting, at least to me, than New Haven was. We'll see how it looks in the morning.

[1]: http://beinecke.library.yale.edu/
