+++
author = "will"
date = "2017-06-21T20:59:36-05:00"
tags = ["personal","vacation"]
title = "Travellin' Man"
topics = ["roadtrip"]
type = "post"

+++

**Road Trip, part the first**

---

We started by passing through the familiar territory of East Texas, and Arkansas, which was pretty uneventful. Then, we crossed the Mississippi river to our first overnight stop in Memphis, including some Beale street BBQ for dinner and a little sight-seeing:

<img src="/img/2017/06/daisy.jpg" alt="Daisy Theater" style="width: 100%; height: 100%"/>

---

Many, many, _many_ miles of Tennessee later, including stopping to pick up a rock that a roadside cliff-face discarded as a souvenir, our second overnight was in Roanoke, VA; against the advice of several family members that think it's _dangerous_ somehow, because of a TV show and because it mysteriously vanished at least once in its history... we were fine though.

---

Many more miles passed; once they stopped traffic on the bridge over the Hudson River to cut across all lanes to park because we didn't have cash and they apparently don't take cards anywhere other than in the main office.

Our last overnight stop is near New Haven, CT so we can have a little drive around look at one of the schools she hopes to get a chance to go to, Yale. The plan is to have a look around the campus, maybe find some place to head down to Long Island Sound so she can touch the Atlantic Ocean, then head on up towards Bahstahn and check out some history before hitting the actual conference we're doing all this for.

---

More to come; stay frosty readers.
