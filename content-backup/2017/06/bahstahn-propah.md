+++
author = "will"
date = "2017-06-24T19:20:04-05:00"
tags = ["personal","vacation"]
title = "Bahstahn Propah"
topics = ["roadtrip"]
type = "post"

+++

## Giving Boston a real chance

When we first got to actual Boston a couple of days ago, we drove around a little to check out the city and get a feel for things.

That kinda sucked. Narrow streets, being tired from the drive up, other terrible drivers, and unfamiliarity with the layout combined to make it pretty stressful.

Today however we decided to give it a real chance, and went to find some bits of the [Freedom Trail][1]. It was much nicer on foot than in-car, even if Zoe started getting a little over-heated.

### We stopped at the USS Constitution:

<img src="/img/2017/06/uss_constitution.jpg" alt="USS Constitution in drydock" style="width: 100%; height: 100%"/>
<div align="center">In dry dock, but still gorgeous.</div>

<img src="/img/2017/06/gun_deck.jpg" alt="Gun deck" style="width: 100%; height: 100%"/>
<div align="center">The gun deck, minus all the guns. Boo.</div>

The ship was in dry dock, clearly, and had all the spars and sails removed for restoration, but it was still impressive as hell. The smell of the sea, the feel of the wood deck where the actual people that fought for our country's freedom also walked was unsettlingly powerful.


### And then walked over to the Bunker Hill Memorial:

<img src="/img/2017/06/bunker_hill.jpg" alt="Imposing as fuck..." style="width: 100%; height: 100%"/>
<div align="center">Sobering.</div>

There's a stairwell up the inside to the little windows where you can see for miles reportedly. We decided to not go up, because Zoe was starting to feel a little sick; I think from the heat and not drinking anything but coffee with breakfast. But it was still good to see. I might try to return before we leave, check out the old North Church, but even if that doesn't happen I feel much better about Boston than when we first arrived. Overall, I'd like to come back with more time and definitely more cash.

[1]: http://thefreedomtrail.org/freedom-trail/maps.shtml
