+++
date = "2017-04-11T18:48:53-05:00"
topics  = ["personal","travel"]  
type    = "post"  
author  = "will"  
title   = "Going on a road trip, apparently..."  
tags    = ["roadtrip"]

+++

So long, involved story about an offspring getting invited to some thing and overspending quite a bit to cover things short; I'm gonna drive from Dallas to Boston this summer.

<img src="/img/2017/04/route.png" alt="Long drive" style="width: 100%; height: 100%"/>

I haven't taken a real vacation in years. I don't know if I remember how. It's all been work for the last three years at least; before that it's honestly hard to remember. Probably should get that checked out.

Anyway, once I get past Little Rock I'll be in unknown territory. Places I've never been or seen. It'll kinda be weird to have time to do what I want, you know? To stop where I want, see what I think is interesting.

It's kindof unprecedented. Probably more to follow.
