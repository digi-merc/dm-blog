+++

topics  = ["networking","sysadmin"]  
title   = "Pfsense on Xenserver"
date    = 2017-11-27T15:22:37-06:00
type    = "post"  
author  = "will"  
tags    = ["bsd","pfsense","xenserver"]  

+++

As I digressed in my [last post](/2017/11/bsd-investigations-part-two/), virtualizing pfSense wasn't as difficult as I expected. From what I'd read online I was afraid it would have some adverse affect on network performance, especially considering most of my "infrastructure" is reclaimed, second-hand, or otherwise cast-off from production use.

It fully appears, however, that these fears were unfounded (standard Spectrum cable, don't judge):


<table>
<tr>
<td>
<div align="center">
<img src="/img/2017/11/physical-pfSense.png" alt="Physical" style="width: 75%; height: 100%"/><br />
Physical 32bit</div>
</td>
<td>
<div align="center">
<img src="/img/2017/11/virtualized-pfSense.png" alt="VM" style="width: 75%; height: 100%"/><br />
Xen Virtualized 64bit</div>
</td>
</tr>
</table>

Barely noticeable, and honestly well within the standard variance of such types of throughput tests.

---

The change did require some cabling rework, obviously. Especially since my VMs are HA-enabled and can change which physical host they live on from time to time...

What I settled on was using one of the unused NIC ports on each host to create a pool-wide "EXT" network to which I can attach interfaces that need to talk directly to the Spectrum equipment, like the WAN interface in pfSense.

<table>
<tr>
<td>
<div align="center">
<img src="/img/2017/11/phy_house_network.png" alt="Physical" style="width: 75%; height: 100%"/><br />
Physical</div>
</td>
<td>
<div align="center">
<img src="/img/2017/11/virt_house_network.png" alt="VM" style="width: 75%; height: 100%"/><br />
HA Xen Guest</div>
</td>
</tr>
</table>

Note the extra "desktop" style dumb switch to separate the physical uplink connections from each host. My primary switch is unmanaged, as well, so no VLANs for me. FML.

---

**Summary**  

I think this has been a pretty worthwhile endeavor. I'll have to wait and see how it recovers when there's a power outage at my apartments, but overall anything I can do to lower the power usage by my little rack I'm all for. That little desktop switch _has_ to use less power than the old Pentium4 firewall device, not to mention the noticeable lack of 4x high-RPM fans running all the time...

Anyway, I'm off to see a special theatrical showing of _Howl's Moving Castle_. See you Space Cowboys.
