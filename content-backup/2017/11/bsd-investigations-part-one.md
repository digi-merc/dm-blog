+++

topics  = ["internets","networking","sysadmin"]  
date    = "2017-11-13T19:21:04-05:00"
type    = "post"  
author  = "will"  
title   = "BSD Investigations, Part One"
tags    = ["internets","bsd"]  

+++

For lots of reasons, lately I've been pretty interested in the various BSD variants, notably [FreeBSD][1]. This has a lot to do with my current choice of firewall software ([pfSense][2]) being based on it, which thanks to recent changes I'm looking at virtualizing instead of running dedicated hardware for. More on that later, I think.

I'll admit, I've always seen the BSD family as better suited to embedded-type applications, probably largely due to pfSense being my primary exposure to it. I decided to remedy that limited exposure. I found long ago when I moved from Windows to Linux that reading about a new OS can only get you so far; you need to dig in with both hands and touch a thing, feel how it jumps at your touch and bites back, to get a good understanding of it. So off to the web I went, and got a couple of variations:

1. The base installer for FreeBSD
  * Aimed at the enthusiast or professional that wants total control over what's on their system, install or compile everything yourself.
  * Like my choice of Linux, Arch, this provides a huge degree of flexibility, but is definitely daunting for neophytes.
2. The most recent [TrueOS][3] iso
  * Aimed at being more of a turn-key corporate-style desktop with a set of pre-installed apps, kindof like RedHat or Oracle did with Linux.
  * Faster initial setup, but takes longer to understand the underpinnings of the system.

VirtualBox handled both of these install setups quite well, and I (not surprisingly) found the TrueOS installer a much simpler way to get things going, especially when it came to configuring X11 and hardware drivers like nVidia or AMD graphics. It also had more of a sense of wonder on first-bootup, whereas the textual installer for FreeBSD was more what I was used to with ArchLinux, a console shell until you can get X to behave...

I've not done a lot with them beyond basic testing, really. They both seem to perform quite well at everyday desktop tasks, web browsing and such. After finding out that my favorite window manager [AwesomeWM][4] is available for both FreeBSD and TrueOS, I think I'm ready to move past the VirtualBox instance and find some real hardware to get things going on. The separated boot spaces and two different methods of full-disk encryption are particularly intriguing... maybe I can think of / find some good uses for it, besides sating my curiosity for a little while.




[1]: https://freebsd.org
[2]: https://pfsense.org
[3]: https://trueos.org
[4]: https://awesomewm.org
