+++

topics  = ["internets","networking","sysadmin"]  
date    = "2017-11-26T02:27:04-05:00"
type    = "post"  
author  = "will"  
title   = "BSD Investigations, Part Two"  
tags    = ["internets","bsd"]  

+++

In my last [post][1], I briefly rambled about my early hands-on with FreeBSD. Today I'm going to expound a bit, and maybe show some pictures. Saucy!

Since my early experiments in VirtualBox showed such ease and promise, I found some physical hardware to continue with:

<img src="/img/2017/11/lilred.jpg" alt="L'il Red" style="width: 100%; height: 100%"/>

That's TrueOS installing all right, on the powerhouse that is the Acer AspireONE Netbook. This mighty beast has a quad-core Atom CPU, with a _full_ 2GB of RAM. Ok, it's kinda shit as laptops go. At least it's 64bit capable, unlike my firewall box (more on that below). Everything worked right out of the box with TrueOS, which I've learned since the last post is really just FreeBSD with some pre-done configuration and package bundling to make it more of a "turn-key" desktop install. Even still, it's pretty telling that some random, low-ass end hardware _all fucking works_, right out of the gate.

I've done some lightweight work with it so far, and it's not complained anymore than it did with Windows 7 (blargh) or way back when I ran ArchLinux on it. It's just shit hardware.

I'll likely use it as a further testbed for vanilla FreeBSD, just so I get some experience with hand-hacking config files etc. Maybe I'll have the presence of mind to take some photos / screenshots.

---

Now a note about another, more familiar use of BSD; pfSense.

In my case, I've had a really reliable, fantastic little device acting as my firewall / network controller for years and years. It's old enough now that there's not even a picture of it on a Google image search, but if you're really curious it's one of [these][2] (PDF brochure). It runs all my house (and test labs) routing, NAT-ing, firewall and SNORT, even hardware-accellerated OpenVPN for when I'm out and about. Sadly, its age is the root of the problem. The CPU in the box that runs all that is a lowly [Pentium 4][3], from the Northwood era, that lacks any semblance of 64bit capabilities.

This hasn't been a problem since I started using it; the 32bit builds of pfSense and its packages have been fine in all but one occasion, when I moved to an InfluxDB-based monitoring stack and the BSD Telegraf agent didn't have a 32bit option. Fine, that is, until Oct 12 of this year when the **2.4 release version of pfSense completely removed 32bit support**. I can still keep using the 2.3.x branch, which _might_ have bugfixes, but for the new builds with their fancy UI improvements and OpenVPN 2.4 and other things like native Telegraf metrics agent packages... I'm outta luck.

Thus, my attempts to virtualize a 64bit install of pfSense on my XenServer host(s). It turns out, it's not so hard to do this, and is (unsurprisingly) closely related to the steps needed to properly run FreeBSD etc under that same hypervisor:

1. Create your VM like always, just using the "Other Install Media" template and install pfSense like normal. I'll leave the details of all the virtual network setup to you.  
2. SSH to your host (or pool master) and run [this little bash snippet][4] I whipped up to disable all the hardware offloading on all the VIFs you assigned to said VM; there should be at least two.  
3. In the pfSense webGUI, hit up System > Advanced > Networking; then check the box for “Disable hardware checksum offload” (might be checked already).

I've got a test of this up and running presently, and will update after I've run on it for a while in regards to the throughput / latency etc for the networking. I've got a fair few devices here, so it should be a pretty thorough test.

Stay frosty.

[1]: https://blog.digi-merc.org/2017-11-13-bsd-investigations-part-one/
[2]: http://www.tyan.com/datasheets/d_nr12b6601_100.pdf
[3]: https://ark.intel.com/products/27447/Intel-Pentium-4-Processor-2_80-GHz-512K-Cache-533-MHz-FSB
[4]: https://gitlab.com/snippets/1684257
