+++

topics  = ["internets","networking","sysadmin"]  
date    = "2017-09-17T16:47:04-05:00"  
type    = "post"  
author  = "will"  
title   = "Malaise, and the fixing thereof..."  
tags    = ["personal","roadmapping"]  
+++

I've spent a long time (feels like a really, really long time), ultimately making cash for someone else, in exchange for a very small fraction of the same. I'll admit, what I do doesn't _directly_ make money for any company; rather I create and streamline processes and tools that let those direct earners and producers do their jobs, thus making measurable monies.

Sometimes, I tell myself I do it so I can feel busy. Other times, it's feeding myself and my kids that's justification enough. This is less and less necessary of late, though, and I feel less and less satisfied by my own reasonings.

Following a time-honored pattern, I started my current gig like I do any other, wide-eyed and optimistic; sure that this time will be the unicorn, the one that I'll stay at forever. Then about two years on, I get that same itch, to find something else, something more interesting, more in need of what I know how to do.

Like all jobs, my current primary has its pros and cons:

### Pros:
**Flexible work schedule.** As long as my stuff gets done they don't really seem to care when, or if I'm even at the office every day. Granted, this opened up a _lot_ after I built a usable, secure VPN for the office(s) and helped move to a cloud-based VoIP service, but the fact remains they're flexible.  
**Choice of tooling** I get to pick (mostly, more further down) how a thing is built or done. There's no committee process with budget oversight and VP approval... well, I guess there kindof is, but it's all one guy besides me really so it's not hard to justify things to him. Unless it's paying for software, which is a bit odd, considering the company loves to get paid for thier software.... `/shrug`.

### Cons:
**Flat hierarchy** This severely limits any chances of "moving up" in the company. There are very few management spots, and they're either owners, partners, or family. So not much chance to grow, there.  
**Disregard for industry gatherings** As many a sysadmin will tell you, you can learn a lot from one of the many conferences such as those put on by O'Reilly or USENIX. The management where I am now, however, has a very biased take on it. Since they are typically exhibitors at such events as IBM's World of Watson or whatever they'll call it this year, they think that the only thing you get is what they offer, sales pitches from a booth. That, or they just don't see my position (and by extension me) as worth investing in. They sure don't mind reaping the benefits if you pay and go on your own and learn tons and bring it back, though.  
**Lack of continued new projects** After sorting out the last 10+ years of "things built by developers", I find there's less and less to do that's actually Sysadmin type tasks. Sure, helpdesk and occasionally HR help, but actually building and automating things? Few and far between anymore. I guess that's what they mean by scripting yourself out of a job. I'm not a developer, Java or otherwise, so the "solution" by management seems to be a combination of fabricating ever crazier ideas for new things, imposition of ridiculous limitations or methods, or outright farming me out on general IT projects to other companies. None of these are what I signed on for.

---

As you can see, my remaining time at the current day job is feeling... limited. To that end, I've started resurrecting the resume, job listing sites, and all the other _accoutrement_ of finding a new day job. I'm also taking the opportunity to look outside my typical geographic radius, to maybe find something either full-time remote or at least in a place that doesn't consider 100deg to be an "average" temperature for 2/3 of the year.

Maybe Connecticut, I liked it there.
