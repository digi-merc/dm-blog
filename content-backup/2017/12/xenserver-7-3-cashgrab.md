+++

topics  = ["sysadmin","xenserver","virtualization"]  
title   = "Xenserver 7.3 Cash-Grab"
date    = 2017-12-15T18:09:47-06:00
type    = "post"  
author  = "will"  
tags    = ["xenserver","xcp-ng"]  

+++

<div class = "hoverable, z-depth-3, card-panel">
<p><b>Update Dec 30 2017</b></p>

<p>The open-source world has responded to this egregious action by re-awakening the old XCP program under a new name, <a href=https://xcp-ng.github.io>XCP-ng</a>.</p>

<p>I'm all for this, and will be watching closely and helping them out any way I can. Anyone else that has the personal bandwidth is encouraged to do so as well.</p>
</div>

After using the open-source Xen hypervisor for a while, I moved to XenServer and loved it for years. In both my lab environments and production use, I liked it exactly because it had feature parity with what I needed from vmWare but without the thousands of dollars in licensing.

I've even got a mid-size software company running on something around a dozen installations of Xenserver, some in pools and some stand-alone. It's become something of the backbone of almost every infrastructure I've built in the last few years. It's become very familiar to me, and much of the day job's infrastructure functionality (rolling upgrades and storage motion, in particular) has come to depend on it heavily.

My going to bat for it, convincing others that we could have reliable, flexible, enterprise-grade systems without spending a whole person's salary for licensing is a lot of why [this blog post][1] from Citrix so thoroughly angers me. In a cutting betrayal, they're literally gutting all the really useful features from the free edition:

>
Dynamic Memory Control  
Xen Storage Motion  
Active Directory Integration  
Role Based Access Control  
GPU Pass-Through  
Site Recovery Manager (Disaster Recovery)  
XenCenter Rolling Pool Upgrade Wizard  
Maximum Pool Size Restricted To 3 Hosts    

I understand wanting to get paid for your software, I really do. **But if you have offered features _for several years_ in a free tier, don't take them away in some desperate attempt to boost subscriptions.** All you're going to do is push people away in the short term, and cause less industry adoption of your platform in the long term.

>
Of the many thousands of customers using the Free edition, we hope that those using it for large deployments (and likely to be using the features above) will consider purchasing a subscription...

And there we have it, direct from the above-linked horse's mouth. I'd thought Citrix was beyond this kind of bait and switch marketing, but I suppose that they were just playing the long con with their open-source "commitment".

After I'm back from my holiday vacationing, I'm going to be strongly investigating the alternatives. Just off the top of my head, [proxmox][2] and [oVirt][3] are very real possibilities for getting Citrix out of my networks entirely.

Hell, after this, I'd almost pay for vmWare's licensing just out of spite. At least they're up-front about just wanting all your money.

Thanks for the memories, XenServer. It's been fun.



[1]: https://xenserver.org/blog/entry/xenserver-7-3-changes-to-the-free-edition.html
[2]: https://www.proxmox.com/en/proxmox-ve
[3]: https://ovirt.org
[4]: https://xcp-ng.github.io
