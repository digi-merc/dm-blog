+++
author = "will"
date = "2017-05-16T14:26:21-05:00"
tags = ["internets","linux"]
title = "(Arch) Linux freezes on second resume..."
topics = ["internets","networking","sysadmin"]
type = "post"
+++

I run Archlinux on most of my workstations, including the trusty [Lenovo t420][1] that I carry around. It's been an amazing little tool, and remarkably functional for close to 6 years now. Try that with a freakin' Macbook.

Anyway, recently I noticed that it would occasionally freeze right after waking up from suspend to RAM, but only the _second_ time I suspended it after a cold-boot. The first suspend/resume cycle would work just fine, but on the second resume it would wake up the display then immediately freeze, no mouse movement, no [TTY switching][2], nothing. It was driving me batty, to the point of pushing me to upgrade the hardware sooner than I'd planned, thinking there was some issue with the venerable old fellow.

As so often happens, I was browsing reddit and found the solution, thanks to a post by [khordes][3] who was having a similar issue. After researching a bit (never trust what you find on reddit without a fair bit of vetting) it turns out there was a kernel and / or driver module update that started causing issues with the `pm_async` functionality built into the Linux kernel:

>Currently, Linux suspends and resumes all the devices in parallel to save time. Disabling async suspend/resume by `echo 0 > /sys/power/pm_async` can be used to check if the suspend/hibernate failure is caused by some unknown device dependency issue. This can also be used to tune driver suspend/resume latency.

Just like the above [excerpt][4] says, turning async suspsed off with the shell command `sudo /bin/sh -c "echo 0 > /sys/power/pm_async"` perfectly fit the bill. You can make this persist after reboot either by making a one-off systemd service or, my choice, with a systemd tmpfile:

```shell
[will@icarus ~]$ cat /etc/tmpfiles.d/no-pm-async.conf
w /sys/power/pm_async - - - - 0
```

And that's it. Suspend/resume now works as expected, through numerous cycles. I had to suspend a couple of times while writing this, actually, as kids and such needed various things like rides from school to work and a trip to the grocery store.

Happy linux-ing you guys.

[1]: https://www.amazon.com/Lenovo-Thinkpad-T420-Professional-Refurbished/dp/B00W4AMWCI
[2]: http://www.tldp.org/HOWTO/Keyboard-and-Console-HOWTO-7.html
[3]: https://www.reddit.com/user/khordes
[4]: https://01.org/blogs/rzhang/2015/best-practice-debug-linux-suspend/hibernate-issues
