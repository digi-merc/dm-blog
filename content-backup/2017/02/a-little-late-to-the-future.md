+++

topics    = ["internets","hosting"]  
date      = "2017-02-25T23:30:00-06:00"  
type      = "post"  
author    = "will"  
title     = "A little late to the future"
tags      = ["hugo","website","golang"]

+++

As some may have noticed, I've completely re-done my consulting website and blog. This comes some three years after I was first introduced to the concept of a static site generator, or SSG, by a former coworker that set up a now apparently un-maintained [personal blog][5] with one of the earliest iterations of Octopress. Better late than never, eh?

Not only are both on much more mobile-friendly layouts, but more excitingly (to me, at any rate) the underlying tech driving the site is no longer the twice-migrated and [somewhat natively vulnerable][4] Wordpress blog and custom landing page (which looked particularly shit on mobile devices).

The whole thing is driven now by one of the hottest things on the internet in the last few years; a static site generator called [Hugo][1]. There's lots of reasons to use static site generators:

1. Simplicity  
  Fewer backend dependencies; no databases, no php (unless you want to use it)  
1. Flexibility  
  No CMS imposed limitations on what can be done, either with layout or integration with other services.  
1. Security  
  No backend means no SQL injection, no admin section brute forcing, no active server-side scripting at all.  

Additionally, there were several reasons I went with Hugo over others like Octopress or Jekyll:

1. It's all markdown based, so writing is just like writing wiki entries or other web content I've been writing a lot of lately.
1. Hugo is, as the name might imply, all written in [golang][2], which is another project I'm working on learning more of.
1. I liked the general feel of it.
1. The availability of themes and ease with which they can be modified didn't hurt, either.

If anyone is curious about how it's all setup, it's pretty straightforward. All my markdown and TOML configurations are open on my [GitLab][3] for all to see; if you have a question that isn't answerable on the Hugo forums or by the readers' own Google-Fu, then my main site has the best methods for contacting me right at the top and bottom.


[1]: https://gohugo.io
[2]: https://golang.org/
[3]: https://gitlab.com/digi-merc
[4]: https://wpvulndb.com/
[5]: http://www.skeptech.org/
