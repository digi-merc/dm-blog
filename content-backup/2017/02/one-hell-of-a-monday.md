+++

topics    = ["job security","whiskey"]  
date      = "2017-02-27T04:47:00-06:00"  
type      = "post"  
author    = "will"  
title     = "One hell of a Monday..."
tags      = ["monday","winxp"]

+++

It's been a hell of a start to the week.  

---

As I was getting ready this morning, about to head out the door to the day job, I got perhaps the worst slack message a systems guy can get:

>Hey… the wired network is down.

Some quick testing showed that most of the office was dead in the water. No internet access (except WiFi, weirdly), which meant that VPN for remote workers as well as our fancy new SIP phone system was completely useless.

As I was already on my way by this point, I continued in and found that two, _just two_ out of maybe 10 VMs had inexplicably halted. They just happened to be the ones that run DHCP and LDAP. I hate to use the word "targeted" but that's kinda how this felt...

---

So that gets squared away, things go back to normal(ish)... a meeting here and there. Then I have a regular office type person come by and ask me to take a look at something that they thought was suspicious. It was simple, Chrome was complaining about a website that still has a SHA-1 signed certificate, but I'm not gonna complain about a user that comes to me when something doesn't feel right online instead of just power-clicking through the warnings.

What stuck in my craw was after reassuring them that this was OK (of course making a note of the site to encourage them to update their certs) one of them whips out a massive, I'm talking dozens of rows, spreadsheet chock full of logins to payment systems and various other SaaS platforms they use... And of course it's stored on Google Drive, probably shared with the other HR-type people.

I was literally aghast. How have these people not been just decimated by data leaks? I can smell on the air another security awareness training round, something on password security and how to properly use a password manager maybe.

---

Then as a reminder of how things were when I started with this company, I had to log into an RDP session for the custom-developed "system of record" time tracking and billing web app; one of the few remaining Windows systems they have in-place, and was greeted with this little gem:  
<img src="/img/2017/02/punisher_screenshot.png" alt="It's 2017 now..." style="width: 100%; height: 100%"/>

---

I'm going home, having dinner, and mixing up a big ol' Jameson and Coke. That's the only way I can think of to salvage my sanity after today.
