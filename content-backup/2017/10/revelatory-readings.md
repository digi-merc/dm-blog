+++

topics  = ["lovecraft,introspection"]  
date    = "2017-10-09T20:17:04-05:00"
type    = "post"  
author  = "will"  
title   = "Relevatory Readings"
tags    = ["personal"]  

+++

Like many around this time of year, I tend to become more introspective. This is, I suspect, due mainly to my family's history of alternative religious beliefs as well as the general human tendency to review, reflect on the past year as it draws near a close.

This personal review process involves reading things I've meant to read, and in general catching up on filling my soul (for lack of a better word) instead of my technical portfolio. Books are infinitely greater than modern e-readers or websites for filling a person with _wisdom_, as well as knowledge. Indeed, I've found that the older the book the higher the effect.

In my reading of a collection of H.P. Lovecraft's works, I came across this rather compelling selection (see if you can guess the work's title in the comments):

> He was, in fine, made sensible that all the world is but the smoke of our intellects; past the bidding of the vulgar, but by the wise to be puffed out and drawn in like any cloud of prime Virginia tobacco. What we want, we may make about us; and what we don't want, we may sweep away.

That last line is particularly poignant for me of late, as I am facing down both another birthday and a profound if not sudden realization that I'm not really happy doing what I'm doing. Indeed, the world I live in is a product of my own will; something I've been loathe to exercise, either out of fear of acceptance or just outright fear, coasting on chance happenings that I neither really wanted or looked for.

**This must change.**

I find myself subsumed by a lusting to see places I've never been, to do things I've never done, with people I have yet to meet... I'm sure some will dismiss this as simply another mid-life crisis by a generic white guy, but that doesn't change the feeling of listlessness, or of growing disgust at the excess around me, by both myself and my fellow humans. I feel much of what I have had in my life is of the latter above, and to be soon swept away. I have to decide what it is that I really want for the next part of my life, post-kids, and start working towards it.

Right now, tentative plans are to go nomad. Get a small-ish travel trailer, decent LTE hotspot, and some kind of job that is full-time remote. Cloud sysadmin, blogger, something that will let me indulge this need to move around, to see more of the world than I have already. This is all a few years away yet, so there should be plenty of time to work out the details, maybe even find someone that would enjoy a similar trip through life.

But that's the real adventure.
