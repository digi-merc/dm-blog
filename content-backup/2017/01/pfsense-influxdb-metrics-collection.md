+++

topics  = ["internets","networking","sysadmin"]  
date    = "2017-01-14T17:55:52+00:00"  
type    = "post"  
author  = "will"  
title   = "pfSense -> influxdb metrics collection"  
tags    = ["grafana","influxdb","linux","bsd","pfsense","monitoring"]  

+++

_UPDATE Oct 20, 2017_  
As pointed out in the comments below, most of this is no longer needed since the official release of the telegraf package for pfSense 2.4 and above.

I'm still leaving it up for posterity.

If any of the pfSense folks read this, some extra configuration options on the settings page for the plugin would be nice to see; but otherwise works like a charm.

---

If you&#8217;re like me, a sexy looking dashboard is a difficult thing to look away from. Time-series data, when properly formatted on a graph is just _so appealing:_

<img src="/img/2017/01/screenshot-metrics-3000-2017-01-14-10-34-19.png" alt="Grafana dashboards are beautiful..." style="width: 100%; height: 100%"/>

&nbsp;

If that doesn&#8217;t get your data collection juices flowing, then something&#8217;s just _wrong_ with you. Seriously. You should get that checked out. Go, make an appointment. I&#8217;ll wait.

Now that you&#8217;re back, making this work with pfSense wasn&#8217;t nearly as complicated as I expected it would be. There&#8217;s not an official pfSense package for the collector agent, but there **is** one for the underlying FreeBSD. The fine folks at [InfluxData][1] have made it really easy to collect data points from all kinds of systems, be they Linux, Windows, BSD, etc and send it to an almost as easily setup InfluxDB host for collection and query. There&#8217;s tons of articles online about setting up Influx / Grafana, so I&#8217;ll leave that up to the readers&#8217; individual Google-Fu and / or the comments to find good guides to that. If there&#8217;s interest then maybe I&#8217;ll write up a little something on it later.

As far as setting up the collector for pfSense, it really couldn&#8217;t be simpler. All you need is a root shell on your pfSense device, and this line:

`pkg add http://pkg.freebsd.org/freebsd:10:x86:64/latest/All/telegraf-1.1.2.txz`

There doesn&#8217;t seem to be a 32bit version in the pkg repos, &#8217;cause why would there be? This is the version at the time of writing, look to the repo for the current URL.

After it&#8217;s installed, there&#8217;s just a couple of things to do that are a little different from regular BSD to configure, enable, and start the Telegraf service and start sending those sweet, sweet datagrams.

`/usr/local/etc/telegraf.conf` is where the whole collector is configured. There&#8217;s two main sections, input and output. The output section is where you configure the InfluxDB (or whatever else) you&#8217;d like to send metrics to, and the input section is where you configure the various things you&#8217;d like the collector to, well, _collect_.

---

### Output(s)
The output(s) can talk to a lot of different systems, but since I'm talking about InfluxDB here, the section we&#8217;re concerned with is `[[outputs.influxdb]]`. It&#8217;s just a matter of setting the URL(s) of the InfluxDB(s) you want your metrics sent to, along with associated logins for said DBs if applicable:

```
[[outputs.influxdb]]
  urls = ["http://10.10.0.15:8089"]
  ...
  database = "telegraf"
  ...
  username = "obscureusername"
  password = "securepassword"
```  

---

### Inputs
As far as inputs, out of the box it&#8217;s already set to collect things like memory, disk, cpu usage etc, but for some reason the network stats collector is commented out. It&#8217;s pretty trivial to enable it, though; just un-comment the line `[[inputs.net]]`.  While you&#8217;re in there, check out the other collectors it has, it can pull in lots of different things that might be of interest to you.

After all that difficult config work, it&#8217;s just as simple as enabling the service to auto-start:

`echo "telegraf_enable=YES" >> /etc/rc.conf`

and starting it:

`/usr/local/etc/rc.d/telegraf start`

&nbsp;

Now just go to either your InfluxDB Admin page or your dashboard and start using the collected metrics in your shiny dashboards to impress the corner office dwellers during the next status meeting:

<img src="/img/2017/01/screenshot-metrics-3000-2017-01-14-11-48-19.png" alt="Query building, redacted-style" style="width: 100%; height: 100%" />

 [1]: https://www.influxdata.com/
