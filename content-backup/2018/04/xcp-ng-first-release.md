+++

topics  = ["sysadmin"]  
title   = "XCP-ng First Release!!!!1!!eleven!!1!"
date    = 2018-04-04T17:29:00-05:00
type    = "post"  
author  = "will"  
tags    = ["xcp-ng","xenserver"]  

+++

>"And in the fullness of time, on the last day of the promised Q1 2018, lo did Olivier deliver unto the world the primal release of XCP-ng.

>And the sysadmins at large saw, and said "It is good.""

The promised day has come and gone, and left us with the [first release of XCP-ng](https://xcp-ng.org/2018/03/31/first-xcp-ng-release/)! I was a little late to the game to be on the "official" mirrors for this initial drop, but I've also got the .iso [here](https://digi-merc.org/xcp-ng/XCP-ng_7.4.iso).

As many others have covered, with the exception of some lingering name changes (still shows XenServer in the top corner of the status console for example) it's really simple to get started with. **I even upgraded a "commercial" XenServer 7.2 testing host to this 7.4-based release with no issues; it didn't even erase the local SR.**

<a href="/img/2018/04/hosts.png"><img src="/img/2018/04/hosts.png" alt="Lab Host List" style="width: 100%; height: 100%"/></a><br />

There are a few things that need working out before they say it's production ready, but this is a damn good first step. Migrating / copying VMs (live or otherwise) from XenServer to XCP-ng works well, but going from XCP-ng to XenServer doesn't. That's not a huge loss if you ask me, I'm not planning on having a mixed environment of some XenServer and some XCP-ng, once it's ready I'll likely move everything to XCP-ng. Fixing the lingering Citrix branding etc, and folks hitting it with harder things than I have here to prove its mettle and build those [RP](http://gta.wikia.com/wiki/Reputation) up are all that really stand in its way, I'd think. There's even talk of a modified XenCenter installer that might be included in the next release; though I don't care so much about that, since [Xen Orchestra](https://xen-orchestra.com) works perfectly well and isn't Windows-only like XenCenter is.

Testing continues, as you can see in these screenshots from my lab's Orchestra instance, I have a number of VMs running on my XCP-ng hardware, both of which were migrated or copied from existing XenServer hosts.

<a href="/img/2018/04/vms.png"><img src="/img/2018/04/vms.png" alt="XCP-ng VM List" style="width: 100%; height: 100%"/></a><br />

There's a lot of potential for the future, to have things be what we would have liked from Citrix that they were unwilling or unable to implement. Things like ZFS, S3, and Gluster storage drivers for instance. But that's on down the road yet. When the XCP-ng automated builds and RPM repos come to fruition I plan to be in on that, as well, so stay tuned if you're so inclined.
