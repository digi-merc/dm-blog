+++

topics  = ["internets","networking","sysadmin"]  
title   = "Terraforming, and Birthdays"
date    = 2018-11-04T15:47:53-06:00
type    = "post"  
author  = "will"  
tags    = ["internets","terraform","aws"]  

+++

It's been a while, but I haven't found anything interesting to write about lately. Until last week, when I got this message in Mattermost PM from our CTO guy:

>
"I'm about to create a ticket that I think will be fun for you (and its something strategic we need within the next several weeks). Go start looking at Terraform"
>

I'd already looked at [Terraform](https://terraform.io) some two years ago when I was evaluating automation tools for use at the day gig, things to help with the increasing tedium of recurring tasks on a growing number of systems in various locations around the world. I decided at the time that it was more overkill than I needed, because I wasn't starting up / destroying environments in cloud providers very often, but needing to run tasks on existing VMs in a more traditional "enterprise" hypervisor/VM structure.

But since this is a software shop wanting to update their demo capabilities, and one that has been slow to buy-in to modern practices until the same CTO reads something about it on Reddit, I get to catch up to the rest of the modern SRE world and build out automated environments and load / configure several proprietary Big Blue software packages inside them. Even beyond the initial AWS instance / security group / EBS volume etc creation, there's lots of moving parts; OpenLDAP and a custom LDIF import to start with. Then some (in)famous BI software and other things that have never (to my knowledge) been attempted to automate installation before.

Should be interesting. Oh, and of course I get until the 20th of Nov to make it all work. Nothing like a deadline, eh?

---

It's also almost my birthdays, again. I'll be 39 and 78 this year, which if I follow family history is about half way through this iteration of "me". I kinda wish I'd accomplished more, but I'm relatively healthy and stable, and have at least a plan for the next few years so I'm better off than most I guess. We'll see how well I can stick to those plans, in the near future. I've already cleared out my storage unit, and am in the process of downsizing, minimizing things in my apartment and life that I don't really need.

We've got client visits all week at the office, so my b-day is just another reason to stay at the house most of the week, in addition to the usual 20+ mile commute and all the traffic issues that come with that in DFW. Plus I don't know if I could stay as sane as I am, forced into business casual every day for a week.

---

I know it's a Sunday, but there's no rest for the sole sysadmin. I'm off to investigate an email delay with G Suite mail relays, pour one out for me brothers.
