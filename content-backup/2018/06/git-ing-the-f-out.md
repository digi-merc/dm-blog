+++

topics  = ["sysadmin","git"]  
title   = "git-ing the F outta Dodge"
date    = 2018-06-04T02:29:17-05:00
type    = "post"  
author  = "will"  
tags    = ["internets","gitlab","github"]  

+++

I've come to amend a lot of my previous thoughts about Microsoft, especially since Win10/Srv2016 came about and undid a lot of the buggery that previous iterations spawned, to say nothing of the newfound love for various Linuxes by Azure etc.

But the news that [Microsoft is buying GitHub][1] is... concerning. Fresh in my mind still is the long history of vitriol from Microsoft's mouth about the open source world; a world that has massive roots in GitHub's clusters; even entire code languages whose package management depends entirely on GitHub.

There has already been a huge jump in people moving away from GitHub to alternatives, which git's naturally distributed repository model helps with. The arguably more important ancillaries, like issue tickets and wiki pages etc, unfortunately aren't so easily moved.

By a lot of accounts, the closest competitor is [GitLab][2], whose products I've been using for some years now (since about the 8.x release range). I was disenchanted with GitHub rather quickly, and I have to say I don't miss it at all. Yes, there was an unfortunate incident last year with GitLab, but in short they learned and adapted to prevent it from recurring. That's really all anyone can do when something bad happens.

If you have similar feelings about Microsoft buying GitHub, and if you have the need, I can totally help out if you or your organization wants to go about setting up your own GitLab instance, or with moving projects to the public instance. I don't make anything from evangelizing GitLab, indeed I've no vested interest in any particular platform or product. I just know what I've found that works the best.

I personally use their free public instance at [gitlab.com/digi-merc][6] for all manner of things. Ansible playbooks, custom Dockerfiles, forks of code repos that have disappeared from places like GitHub for one reason or another, even this very website's code repository is there and automatically builds and deploys to my web host every time I push a new post thanks to their pretty kick-ass Docker-based CI/CD features. One reason I chose to use them was unlimited private repositories, where with GitHub you have to pay to even get _one_. I'm not putting my ansible playbooks etc out in a public repo, to say nothing of work I might do for clients...

I also run a couple of self-hosted GitLab instances for a mid-size software shop, and by and large they really like it. Getting stodgy old devs to give git a try after decades of SVN use is always fun, but other than the ever-present people aspect at this one shop it's a solid product with a lot of thoughtful features from a sysadmin viewpoint. Automatic backups to local or remote storage (like NFS or S3), project importing and loads of integrations like Slack (or my favorite, MatterMost which comes bundled with GitLab if you so desire), loads of user auth options, and you can even run your own Docker repositories under the same user auth umbrella... [they do a better job of describing all it can do than I do though][3].

Since GitHub's purchase made the news, GitLab have already seen a [10x jump in new repo creation, just in the day (not even) or so since the "announcement" was made][4]; they are also having a pretty steep discount on the paid "enterprise" versions if your org needs some of those features. There's a pretty good import feature, especially if you're coming from GitHub things "just work".

So if after weighing your options you're looking to move your organization or projects away from GitHub for whatever reason and need someone with experience doing so, [hit me up][5] and we'll get you sorted.



[1]:https://www.bloomberg.com/news/articles/2018-06-03/microsoft-is-said-to-have-agreed-to-acquire-coding-site-github
[2]:https://about.gitlab.com/
[3]:https://about.gitlab.com/pricing/
[4]:https://about.gitlab.com/2018/06/03/movingtogitlab/
[5]:https://digi-merc.org
[6]:https://gitlab.com/digi-merc/
