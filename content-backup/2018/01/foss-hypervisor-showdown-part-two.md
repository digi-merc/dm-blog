+++

topics  = ["networking","sysadmin"]  
title   = "FOSS Hypervisor Showdown, Part Two"
date    = 2018-01-04T18:56:38-06:00
type    = "post"  
author  = "will"  
tags    = ["ovirt","proxmox","nutanix","xcp-ng"]  

+++

In part one of this little series, I covered in some detail my investigation of oVirt as a potential replacement for XenServer in my environments. While I feel it is a very robust product, the lack of functional .ova import / export is a deal breaker for most of my use cases, since the day job has a lot of developers that depend on that.

Today, we're going to look at [Proxmox VE](https://www.proxmox.com/en/proxmox-ve), another KVM/LXC based platform but based on Debian this time instead of CentOS.

---
### Hardware
As a refresher, the hardware I had available to test with is a bit older, but still fairly typical of a lot of environments:

* HP ProLiant DL160 G5  
* 2x Intel(R) Xeon(R) E5520 @ 2.27GHz  
* 32GB DDR3 ECC  
* 4x 1tb Seagate Barracuda in RAID5 on an HP SmartArray g6 controller.  
* 2x Intel GBE nic

---
### Proxmox VE
Proxmox is another of the platforms that give away the software, but enterprise level support is on a tiered for-pay system. Thus, obtaining and prepping the installer medium was pretty simple, and they even have good [documentation](https://pve.proxmox.com/wiki/Main_Page) for the process.

#### Installation / configuration
This was even simpler than CentOS, and minus all the package-level work necessary with oVirt. After a bit of churning, it'll reboot then you can login to the web UI using `root` and the password you set during installation.

The UI is less complex out of the box than oVirt, and seems more familiar to those that might be accustomed to the XenCenter application layout from XenServer:

<a href="/img/2018/01/pve-dash.png"><img src="/img/2018/01/pve-dash.png" alt="Quick" style="width: 100%; height: 100%"/></a><br />

It also does some of the work for you up-front, like creating a local storage repo. It didn't create an ISO storage repo, but that's as easy as selecting the pre-populated datacenter, then "Storage" and selecting the right option from the "Add" drop-down:

<a href="/img/2018/01/pve-storage.png"><img src="/img/2018/01/pve-storage.png" alt="Easy" style="width: 100%; height: 100%"/></a><br />

In my case, it created a subfolder tree in my existing NFS mount, but it wasn't nearly as complicated as the one oVirt did and it offers a webUI to upload images if you don't already have any in there, which is neat.

#### Build a VM from scratch
Creating a VM is straightforward, as well, with a very nice wizard to guide you through the options. Not quite as configurable as oVirt's VM creation, but it covers the basics well:

<a href="/img/2018/01/pve-vm-create.png"><img src="/img/2018/01/pve-vm-create.png" alt="Friendly" style="width: 100%; height: 100%"/></a><br />

Provisioning took less time than on oVirt, and installation in general went faster as well. Missing, however, was the neat SPICE option for guest consoles; the only option in Proxmox seems to be based on noVNC. It's functional, and runs over the same self-signed SSL connection that the whole webUI runs under, so at least it's encrypted:

<a href="/img/2018/01/pve-vm-console.png"><img src="/img/2018/01/pve-vm-console.png" alt="Functional" style="width: 100%; height: 100%"/></a><br />

SPICE is an option in the console drop-down, but it's greyed out. Maybe some kind of plugin that needs separate installation? Anyway, VM OS installation took less time than it did in oVirt, likely due to direct local disk I/O instead of the wonky loopback NFS stuff I had to do with that other platform.

#### Import / Export
Once again, this feature (or lack thereof) is the decider for whether or not I can justify recommending we replace XenServer with a new thing.

And once again, it's depressingly convoluted to do either.

[Importing](https://www.jamescoyle.net/how-to/1218-upload-ova-to-proxmox-kvm) isn't simple at all, including SSH-ing to the host to manipulate the image. Exporting is a nested mess of a process, involving exporting the disk image as a VMDK (further complicated if you built it on the built-in LVM storage instead of file-based images like QCOW), then using VBox or vmWare tools or whatever to build an .ova around it on a workstation... too much of an ask for most Java devs.

---
### Conclusion
As fun as it's been to get a little more handsy with these open-source hypervisors, the across the board lack of support for OVA formats heavily limits their use in my environments. Both seem quite usable, otherwise, and both have a large amount of community support online from people using them with high levels of success. Of the reviewed options I'd have to go with oVirt simply because of the much higher level of customization, on both the hosts and the VM configurations; I can see where the quicker, simpler setup of Proxmox could have a certain appeal as well.

That said, considering the need for import / export as a usable feature, I'll be sticking with XenServer 7.2 and just ignoring the upgrade to 7.3 until we see how the whole [xcp-ng](https://xcp-ng.github.io) project plays out. I'm not much of a developer, but I'll be offering them my services in any capacity they might can use. At the very least, I'll probably mirror their yum repo somewhere, maybe even host an official one if they need it.

I hope everyone reading this that was put off by the whole XenServer 7.3 feature stripping situation considers giving them any help they can. Until next time, see you Space Cowboys.
