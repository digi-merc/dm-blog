+++

topics  = ["firearms",""]  
title   = "Physical Security Matters, Too"
date    = 2018-02-14T18:42:53-06:00
type    = "post"  
author  = "will"  
tags    = ["rangeday","firearms"]  

+++

_or_ **Shitty City; bang, bang.**

Like a lot of sysadmin type folks machines are fascinating to me, including the ever controversial firearm. Shooting sports has always been an interest of mine, my family were outdoorsy types and always had them for both food gathering and defense. Living in Texas enables, nay _encourages_ (almost requires) this hobby, as does having lived / worked in remote or sketchy enough places that your life might literally depend on them from time to time.

At any rate, a new range recently opened up not far from the house, so I stopped by to check it out since my previous favorite closed down. I had also for a little while been looking for something like the [Ruger LCP](https://ruger.com/products/lcp/) for my every day carry, lightening my load so to speak. So while I was at the new range I took the time to get a little handsy with some of the various pocket gats they had on display.

I discarded the idea of a [Beretta Pico](http://www.beretta.com/en-us/pico/) right away, the trigger pull was awful and the whole thing just felt bad in the hand.  

There were [Tarus Public Defenders](https://www.taurususa.com/product-details.cfm?id=693&category=Revolver) and [Bersa Thunders](https://bersa.eagleimportsinc.com/bersa/firearms/thunder-series) that felt amazing but were ultimately bigger than I wanted.  

I heavily considered going for the [LCP II](https://ruger.com/products/lcpII/models.html) for its better trigger. Then I saw, down in the corner of the used gun case, there were also a pair of 1st gen LCPs, one with a custom adjustable trigger and a CrimsonTrace laser... for $189.

So I bought an LCP.

<img src="/img/2018/02/thespread.jpg" alt="My little collection" style="width: 100%; height: 100%"/>
<div align="center">Bottom right, it fits in well enough with the group...</div>

And it turned out so much better than I thought it would. The battery was dead in the laser, but that was an easy fix. And not only is the trigger adjustable, it came with 3 extra recoil springs of various tensions that the previous owner had apparently bought but never tried out. Also in the box were 3x magazines (two of which now have 1" extensions on them, one is in the photo above) and the original trigger.

A couple weeks lapsed before I got time to take it to the range, but by then my grip extensions had come in. It definitely has more muzzle flip than my .45 Sig does, which I figure is due to the diminutive frame and relatively soft 9lb recoil spring that came factory. The initial adjustments I did to the laser in my apartment were pretty much dead-on for 7-10 yards though, so I figure this isn't so bad. I kept the grouping smaller than a torso, at least:

<img src="/img/2018/02/number6.jpg" alt="Meh." style="width: 100%; height: 100%"/>
<div align="center">The two outside the orange were my oldest son missing #5 and #4 on the paper, rather badly, with a .45 Kimber Custom II</div>

Overall I'm happy with the little guy. It's super easy to carry, even in a pocket with the right holster, which was the whole point. I did put the 11lb recoil spring that the previous owner included in it; it feels a good bit stiffer to rack now. I'll probably see how it affects recoil and thus sight picture recovery this weekend since I got a couple range passes when I bought it.

I'm off to play some Rust now I think, ya'll have a good one.
