+++

topics  = ["personal"]  
title   = "Pattern Recognition"
date    = 2021-01-11T00:04:41-06:00
type    = "post"  
author  = "will"  
tags    = ["personal"]  

+++

A lot happened last year. And also very little.

It was an odd year.

A prevailing office mentality that so shunned my own move to fully remote status mid-2019 flip-flopped and mandated everyone work from home indefinitely, in fact we're still doing it with no noticeable detriment to productivity or profitability. In further fact, we got profit sharing checks for the first time in almost two years after huge fears mid-spring. I haven't had to fly to DFW for office time since Feb 2020.

But it's not all work. I ignored this very blog for the entire calendar year. Didn't write a fucking word. And it's not like nothing happened, I just didn't apparently think anything was worth talking about. Well here I am talking about it all after the fact. Shouting to the nobody that ever reads this.

I bought another Jeep, a YJ for ~$500. It was drivable, but pretty rough. I proceeded to spend close to $1500 on frame repair and a new top, brakes, and tires. It's almost a fully functional vehicle now, and a decent backup for when I break the Cherokee. Like I almost immediately did. Fucking stumps and weak control arm brackets. More welding for me, I guess.

My oldest two children followed me after I moved to North Arkansas, one of them bringing his fiancée along with. I love my kids, but living with them again as adults was... occasionally frustrating. Still is, a bit. It's hard to develop yourself as an independent entity (much of the point of moving here myself) with constant context switching between "normal" and "dad" modes.

Despite that, I managed to meet someone towards the end of the year, someone completely unlike anyone I've ever "known", biblically or otherwise. I'm not going to go into details about her, she's perhaps even more personally private than I am. It's being interesting, getting used to having someone in my life again. I've become accustomed to not necessarily freedom, but not having to take another's context and view under advisement when deciding to do a thing. Also, cultural differences are a bigger divider than I had previously expected. Maybe I'll cover that in more detail at some point, but considering the fruitlessness of expounding on it here with my virtually non-existent readership (much thanks to the 4 people from China and two Hungarians, according to Google Analytics) I don't know if that will ever happen.

I'm also not sure what the future holds. I don't suppose anyone really is sure. This year has thrown a lot of things for a loop. I don't know how things will progress with this woman. I don't know if we'll ever go back to the traditional office-based workplace, or if we'll stay fully remote indefinitely. I don't know if I'll ever have a reason to go back to DFW again. I don't know a lot. Other than that my contacts are dry and I'm going to go to bed.

Maybe I'll write more this year than I did in the last two. Maybe this isn't the beginning of a pattern of ignoring myself and my outlets.

I dunno.
