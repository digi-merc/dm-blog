+++

topics  = ["personal","sysadmin"]  
title   = "Almost a year, really?"
date    = 2019-09-18T11:42:58-05:00
type    = "post"  
author  = "will"  
tags    = ["aws","roadmapping"]  

+++

It was pointed out recently that I haven't written anything here in almost a full year... well, as you might expect a fair bit has changed:

#### Firstly, I bought a house and moved ~400 miles.

There was a lot to this decision, clearly. I don't speak a word without thinking through the repercussions, so something of this magnitude was carefully considered.

1. My two oldest offspring (the ones living with me full time) have graduated high school, and moved on to their own lives, mostly. So I didn't need a $1300/mo three-bedroom apartment anymore.
2. My father turned 70 last January, and though he doesn't want to admit it he needs some help with things, especially his retirement gig driving a water delivery truck, until my oldest son is ready to take that over for him within a year or so.
3. My ability to tolerate the increasingly toxic (to me, at least) nature of rampant urbanity in the DFW area was waning. Still is, whenever I have to go back for the day job... everyone seems to hate the very idea of other people. Probably because there's so many in such a densely packed area. My mindset also doesn't seem compatible with the prevailing majority; I don't care about what the newest "sexy" mobile phone is, or a lot of other things I truly consider to be meaningless social posturing.
4. Housing here is so much less expensive. I bought a three bedroom, multi-level house on a double corner lot (about 1/3 acre) for ~$85k. I have literally one neighbor that I can see from my house. The same house in DFW would have been at least $250k, and would have likely come with HOA fees as well.

#### Lots of changes to network systems

I moved around a lot of systems / services, things long overdue:

1. My traditional colo-based systems have been all moved to cloud platforms. Mostly to AWS, but some others are mixed in for good measure. Moving further away from the physical location was part of the impetus for this, but just as much was the increasing age of the hardware and my disinterest in purchasing something else to replace it, and all the management overhead that goes with that.
2. Hosting for this site has moved to GitLab Pages. Since all the code for it already lives there, it just made sense. Especially for the price.
3. All my email runs through G Suite now. I moved all the DKIM/SPF/DMARC stuff as well, I hate spam. Running even a modest instance somewhere to continue using my self-hosted Zimbra was running me ~$35/mo, compared to $7/mo for G Suite. Kindof a no-brainer, especially since I get Gmail's much better inbound spam filters as part of the deal.

#### Some personal improvements

As a result of all the cost of living decreases as well as other factors I've been working on myself a little more, something I haven't had time to do in a couple decades at least:

1. I've lost weight. Not sure how much exactly, because I pointedly don't own a scale, but enough that I had to buy pretty much a new "wardrobe", meaning several new pairs of jeans.
2. While in the process of moving, I discarded much that I had no real desire to keep. This was as much a mental exercise as physical, I've returned many things to their proper owners, and outright removed much cruft from my life.
3. Most recently, I took and passed a certification test for AWS Cloud Practitioner. This is especially high in my mind, not only because it's recent but because I did it without any assistance or encouragement of any kind from the day job. I'm not sure whether they don't see my _position_ as worth the investment, or if it's _me personally_, but either way they've repeatedly shown that they have no interest in helping me further my skills or career in any way. Not in so many words, of course, but actions speak louder don't they?

Anyway, that's the biggest updates from the last several months, for those few of you that might actually care enough to read all the way through this. Happy whatever you do-ing.
