+++

topics  = ["code","portfolio"]  
title   = "Public sites or other things I've built."
date    = 2025-01-06T00:04:41-06:00
type    = "page"  
author  = "will"  
tags    = ["code", "portfolio"]  

+++

## Professional Portfolio
Fully managed domains, from registrar to DNS to email / Google apps management, website design and hosting.

#### Yellville Dipsticks
https://yellvilledipsticks.com

#### AR Water Wagon
https://arwaterwagon.com

---

Below are some of the various little things I've done that might not be obviously documented elsewhere.

1. https://mc1.digi-merc.org Map for personal Minecraft server
1. https://charliehateschairnoise.com Joke website to playfully annoy one of my offspring