+++

topics    = ["networking","sysadmin"]  
date      = "2016-11-14T17:16:34+00:00"  
type      = "post"  
author    = "will"  
title     = "Make XenServer 7 do meaningful things"  
tags      = ["datacenter","linux","redhat","vmware","xenserver"]  

+++

<div class = "hoverable, z-depth-3, card-panel">
<p><b>Update Jan 31, 2018</b></p>

<p>This is verified (by me, at least) to work on both the official XenServer 7.2, and with the experimental <a href="https://xcp-ng.github.io">xcp-ng</a>. I've also semi-automated the process with these <a href="https://gitlab.com/digi-merc/dell-osm-for-xenserver">Ansible bits</a> for new hardware / pool upgrades.</p>
</div>

---

Don&#8217;t get me wrong, [XenServer 7][1] is a huge improvement over previous versions, and still my product of choice for those that don&#8217;t want to pay a literal fortune for vmWare licensing. There are however a few places that the default config / included softwares are a bit lacking, so here&#8217;s what I do to build out a new Xen host, after doing all the hardware-specific stuff like RAID config and of course actually installing XenServer:

  1. **Install Dell OpenManage** (makes remotely managing Dell hardware, my favorite, a little easier)
      * `wget -q -O - http://linux.dell.com/repo/hardware/dsu/bootstrap.cgi | bash` This runs the Dell OM repository configuration, as well as imports the necessary GPG keys etc.
      * Fix the broken base CentOS repository line in `/etc/yum.repos.d/Base-CentOS-Base.repo` to be `baseurl=http://mirrors.kernel.org/centos/7/os/x86_64/`. This step is necessary because the Dell software has some requirements that are in the CentOS base repo; it&#8217;s nothing that conflicts with the standard XenServer installation packages, though. I like the kernel.org mirror, you can use whichever one is your favorite.
      * Install OpenManage: `yum --enablerepo=base install srvadmin-all` &#8211; Note that this only enables the base repo for this one installation, and that I didn&#8217;t enable it across the board above; this helps avoid future issues if an update later on supercedes a XenServer core package.
      * Update iptables to allow the incoming connections to the web UI by adding this line: `-A RH-Firewall-1-INPUT -m conntrack --ctstate NEW -m tcp -p tcp --dport 1311 -j ACCEPT` to `/etc/sysconfig/iptables` after the line that has ESTABLISHED,RELATED in it. Don&#8217;t forget to restart iptables after making the changes.
      * Enable and start the OpenManage services: `/opt/dell/srvadmin/sbin/srvadmin-services.sh enable && /opt/dell/srvadmin/sbin/srvadmin-services.sh start`
      * verify login works via https://[server_ip]:1311 &#8211; Note the http**S**; login will be whatever your XenServer root login was set to during installation.
  2. **Enable SNMP** for monitoring systems etc
      * Add the following line to the same `/etc/sysconfig/iptables` we edited above, I typically put it above the last line in the file: `-A RH-Firewall-1-INPUT -s [monitoring_IP] -p udp -m udp --dport 161 -j ACCEPT`. Replace the[<monitoring_IP] bit with the IP of your monitoring system, to only allow that system to access SNMP.
      * Edit `/etc/snmp/snmpd.conf` to meet your monitoring needs; however the defaults of CPU, memory, and FS usage work just fine for most of my cases.
  3. **Update all the things**
      * Every time I install a new XS host, the most time consuming thing I have to do is get all the updates since the ISO was made installed. Sure, there&#8217;s time savers like cloning etc, if you have to do many at once, but for more one-off setups one of the fastest ways I&#8217;ve found is to use [Xen Orchestra][2]&#8216;s app in lieu of the standard XenCenter to apply updates in bulk to new hosts. It&#8217;s a little different experience, but makes the initial bulk apply much easier, especially if the hosts aren&#8217;t part of a pool.

That&#8217;s really about it; other than environment-specific stuff like remote storage repositories for disk images or setting up domain authentication for management users, XenServer should now just quietly sit in the back and do its job, like all good software tools. I recommend setting up some kind of recurring maintenance schedule to install updates etc, in particular on production systems, but if you&#8217;re reading this you honestly should have that going already.

If you have a post-install thing you do that helps with Xen management or monitoring, feel free to add it below to help spread the word.

 [1]: http://xenserver.org/overview-xenserver-open-source-virtualization.html
 [2]: http://xen-orchestra.com
