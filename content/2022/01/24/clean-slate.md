+++

topics  = ["personal"]  
title   = "Clean Slate"
date    = 2022-01-24T00:04:41-06:00
type    = "post"  
author  = "will"  
tags    = ["personal", "code"]  

+++

It's been long enough since I wrote anything here that it only made sense to start over, wipe the blog slate clean. It 
had become a dumping ground of poorly written stream-of-consciousness type of posts that meant nothing to anyone, least of 
all myself.

I've decided to re-dedicate this as a bit of a personal project log. There won't be any real set structure, or cadence 
of posts. If I think about it, I'll document whatever side project I've worked on recently here, with pictures or video
or whatever.

In the meta spirit, this first post is about the site itself since that's the project I've been working on most recently.
A lot of the grunt work for the site was done by past me, I just had to re-structure the content and post tagging etc to
reflect the new "mission" of the thing. 

It's still based on [Hugo][1], like most sites I've built recently. I like the concept, the simplicity, and outright speed
of the static site generator movement, as well as the ease of hosting / integration with free CI/CD platforms like [Gitlab][2]
to keep things organized, consistent, and simple to update. The project home for this blog itself (including all the "magic"
for the Hugo build and artifact push to their "pages" site hosting) can be found [here][3].

In the future, expect better of me with photos etc of some of the more physical projects I work on. Just *right now* there's
a mini Kubota tractor, a 1969 Army 5-ton 6x6 truck, and a former pop-up camper I'm converting into a kayak carrier trailer
all sitting in my yard waiting on warmer weather to put wrenches / welder to... so much more to come.

[1]: https://gohugo.io
[2]: https://gitlab.com
[3]: https://gitlab.com/digi-merc/dm-blog