+++

topics  = ["personal"]  
title   = "Slow but Steady"
date    = 2022-08-17T00:04:41-06:00
type    = "post"  
author  = "will"  
tags    = ["personal", "code"]  

+++

I've been remiss in updating here, but other than incredible heat outside there's not been
a ton going on. 

Lots of small progress on things that nobody will care about, upgrades to the house (security
cameras, wifi coverage upgrades, etc) and ongoing vehicle maintenance. I did "finish" my kayak trailer,
but so far have only used it once.

If anyone knows where I could find a 20' shipping container for a reasonable price let me
know. Since I started looking just a few months ago they've nearly doubled.

Anyway, mostly this is a keepalive type post, nobody will likely read or care about it. Have a good one.