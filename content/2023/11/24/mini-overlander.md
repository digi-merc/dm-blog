+++

topics  = ["personal","overlander"]  
title   = "Mini Overland Camper"
date    = 2023-11-24T00:04:41-06:00
type    = "post"  
author  = "will"  
tags    = ["personal", "overlander"]  

+++

I see it's been a very long time since I've updated anything here. I've started a new project, a little more hands-on than some of my previous, more online-tech related ones.

I made what much of my family considers to be a somewhat unwise purchase, a 1958 utility trailer:

<img src="/img/2023/11/Snapchat-33578073.jpg">

I'm planning on making it into a custom mini-overland style camper for some solo travel shenanigans in the not so distant future. I already undid some previous owner after-market mods,
removed some junky lights, replaced some of the unused originals and re-wired them to be a little better than the inevitable twisted wires / hardened electrical tape job that had been
in place for years:

<img src="/img/2023/11/Snapchat-587186717.jpg">

That's all I've been able to get going so far; holidays, weather, and shorter winter days all conspire against me. I'll try to keep updating with progress as it happens, though.