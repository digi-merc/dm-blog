+++

topics  = ["personal","overlander"]  
title   = "Mini Overland Camper Update"
date    = 2025-01-06T00:04:41-06:00
type    = "post"  
author  = "will"  
tags    = ["personal", "overlander"]  

+++

A long overdue update to the mini-camper workings...

I've made some progress on retrofitting a more modern suspension onto the rather basic chassis. I started with a cutting torch to remove the existing... <i>axle</i>, then some generic steel rectangular tubing to reinforce the ancient existing frame and give a platform to mount an [axle-less setup from Timbren](https://timbren.com/products/asr2khds03-axle-less) (pricy, but well built):

<img src="/img/2025/01/cutoffs.jpg"> <img src="/img/2025/01/marketplace-steel.jpg"> <img src="/img/2025/01/timbren-bits.jpg">

Then some stuff that I only documented on snapchat during the process, to fit up and attach the new suspension, with [electric brakes and hubs that fit the same wheel bolt pattern as my tow Jeeps](https://timbren.com/products/94545pr-asr2khds01-asr2khds02-asr2khds03-asr2khds04-asr2khds05-asr2khds09-asr1200s04-asr2000s02-asr1thds01-asr1thds02-asr1thds03-asr1thds04-asr1thds05-asr1thds09-asr3500s04-asr3500s05-asr3500s06-brake-hub):

<img src="/img/2025/01/Snapchat-1253202221.jpg"> <img src="/img/2025/01/Snapchat-1925505281.jpg"> <img src="/img/2025/01/Snapchat-1373157297.jpg">

And now with all the reinforcement fully welded and the new suspension bolted on, I'm calling the "make it a better trailer" part done for now, meaning it's time to start on the "make it have decent living quarters" part. I'm still in the materials gathering phase, but slow progress is being made on this and a couple other projects at the same time. 

Those are still a secret for now, but the 4 people that read this probably have already seen it around the house. More to come when I have the time to type it all out.